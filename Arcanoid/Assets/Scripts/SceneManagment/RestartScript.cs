﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
public class RestartScript : MonoBehaviour
{
    [SerializeField]
    private Button _restart;
    private void Start()
    {
        _restart.onClick.AddListener(Restart);
    }
    private void Restart()
    {
        SceneManager.LoadScene("GameScene", LoadSceneMode.Single);
    }
}
