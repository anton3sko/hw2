﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class MainMenu : MonoBehaviour
{
    [SerializeField]
    private Button start;


    void Start()
    {

        start.onClick.AddListener(begin);
    }


    private void begin()
    {
        Debug.Log("Start");
        SceneManager.LoadScene("GameScene", LoadSceneMode.Single);


    }
}
