﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using com;
public class LifeCounterScript : MonoBehaviour
{
    private int Lifes;
    void Start()
    {
        Lifes = 3;
    }

    void OnCollisionEnter2D(Collision2D collision)
    {
        Debug.Log("HIT");
        Debug.Log("collision name = " + collision.gameObject.name);

        if (collision.gameObject.name == "Ball")
        {
            Lifes--;
            EventManager.DispatchEvent(EventManager.DAMAGED, null);
            if (Lifes == 0)
            {
                Destroy(collision.otherCollider);
                Destroy(gameObject);
                EventManager.DispatchEvent(EventManager.DESTROIED, null);
            }
            

        }


    }
}
