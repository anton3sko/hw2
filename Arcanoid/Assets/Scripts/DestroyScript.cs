﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using com;

public class DestroyScript : MonoBehaviour
{

    
    void OnCollisionEnter2D(Collision2D collision)
    {
        Debug.Log("HIT");
        Debug.Log("collision name = " + collision.gameObject.name);

        if (collision.gameObject.name == "Ball")
        {
           
            Destroy(collision.otherCollider);
            Destroy(gameObject);
            EventManager.DispatchEvent(EventManager.DESTROIED, null);

        }


    }
}
