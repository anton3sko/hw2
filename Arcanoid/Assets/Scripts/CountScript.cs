﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using com;

public class CountScript : MonoBehaviour
{
    static private int currentScore;
    [SerializeField]
    private Text _scoreText;


    void Start()
    {
        
        EventManager.AddListener(EventManager.DESTROIED, OnDestroied);
        EventManager.AddListener(EventManager.DAMAGED, OnDamage);
        currentScore = 0;
    }
    void Update()
    {
        HandleScore();

    }
    private void OnDamage(EventData eventData)
    {
        currentScore += 3;
    }
    
    private void OnDestroied(EventData eventData)
    {
        currentScore += 5;
        if (currentScore == 135)
        {
            SceneManager.LoadScene("Victory", LoadSceneMode.Single);
        }
    }
    void HandleScore()
    {
        _scoreText.text = "Score: " + currentScore;
    }
}
