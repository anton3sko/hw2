﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BallScript : MonoBehaviour
{
    private float speed = 10f;


    void Start()
    {
        GetComponent<Rigidbody2D>().velocity = Vector2.one.normalized * speed;
    }

}
