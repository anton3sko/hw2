﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BoxesScript : MonoBehaviour
{
    [SerializeField]
    private Transform prefab;
    [SerializeField]
    private Transform prefabRed;
    void Start()
    {

        for (int i = 0; i < 10; i++)
        {
            Instantiate(prefab, new Vector3(-8 + 2 * i, 2, 36), Quaternion.identity);
        }
        for (int i = 0; i < 10; i++)
        {
            
            Instantiate(prefabRed, new Vector3(-8 + 2 * i, 3, 36), Quaternion.identity);
        }
    }
}
