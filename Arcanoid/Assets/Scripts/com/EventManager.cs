﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

namespace com
{
    // создавать события и возможность на них подписаться
    public class EventManager
    {
        // event name
        static public string CLICK = "click";
        static public string MOVE = "move";
        static public string CHANGE_DROPBOX = "changeDropBox";
        static public string DESTROIED = "destroied";
        static public string DAMAGED ="damaged";


        // pool all callbacks
        static private Dictionary<string, List<Action<EventData>>> _pool = new Dictionary<string, List<Action<EventData>>>();

        static public void AddListener(string eventName, Action<EventData> callback)
        {
            if(_pool.ContainsKey(eventName)) {
                _pool[eventName].Add(callback);
                return;
            }
            List<Action<EventData>> l = new List<Action<EventData>>();
            l.Add(callback);
            _pool.Add(eventName, l);
        }

        static public void DispatchEvent(string eventName, EventData data)
        {
            UnityEngine.Debug.Log("DispatchEvent");
            // check is event
            if(!_pool.ContainsKey(eventName)) {
                return;
            }
            // call all actions
            /*foreach(var act in _pool[eventName]){
                act(data);
            }*/
            List<Action<EventData>> lAction = _pool[eventName];
            for(int i = 0; i < lAction.Count; i++){
                lAction[i](data);
            }
        }

        static public bool RemoveEvent(string eventName, Action<EventData> callback) {
            // check is event
            if(!_pool.ContainsKey(eventName)) {
                return false;
            }
            // get all actions by eventName
            List<Action<EventData>> lAction = _pool[eventName];
            for(int i = 0; i < lAction.Count; i++) {
                if(lAction[i] == callback){
                    lAction.RemoveAt(i);
                    return true;
                }
            }
            return false;
        }
    }
}